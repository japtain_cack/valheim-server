[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/valheim-server?style=for-the-badge)](https://gitlab.com/japtain_cack/valheim-server/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/valheim-server?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/valheim-server/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/valheim-server?style=for-the-badge)](https://gitlab.com/japtain_cack/valheim-server/-/issues)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)

# valheim-server
Run a Valheim dedicated server in a Docker container.

This uses steamCMD to automatically update your server software.

This Dockerfile will download the Valheim dedicated server app and set it up, along with its dependencies.

If you run the container as is, the `game` directory will be created inside the container, which is inadvisable.
It is highly recommended that you store your game files outside the container using a mount (see the example below).
Ensure that your file system permissions are correct, `chown 10000:10000 mount/path`, and/or modify the UID/GUID variables as needed (see below).

It is also likely that you will want to customize your `start.sh` file.
To do this, use the `-e <ENVIRONMENT_VARIABLE>=<value>` for each setting in the `start.sh` file.
The `start.sh` file will be overwritten every time the container is launched. See below for details.


# Run the server
## Docker
Use this `docker run` command to launch a container with a few customized `start.sh` options.
Replace `<ENVIRONMENT_VARIABLE>=<VALUE>` with the appropriate values (see section "Server properties and environment variables" below).

```
docker run --name valheim -it --rm \
  -p 28015:28015 \
  -v /home/$(whoami)/valheim:/home/valheim/server \
  -e STEAMCMD_VALIDATE="validate" # can be "" or "validate" \
  -v /opt/valheim/world1:/home/valheim/server \
  -p 2456-2458:2456-2458/tcp \
  -e VALHEIM_NAME="valheim" \
  -e <ENVIRONMENT_VARIABLE>=<VALUE> \
  registry.gitlab.com/japtain_cack/valheim-server
```

### Set selinux context for mounted volumes

`chcon -Rt svirt_sandbox_file_t /path/to/volume`

### Additional Docker commands

**kill and remove all docker containers**

`docker kill $(docker ps -qa); docker rm $(docker ps -qa)`

**docker logs**

`docker logs -f valheim`

**attach to the valheim server console**

Use `ctrl+p` then `ctrl+q` to quit.

`docker attach valheim`

**exec into the container's bash console**

`docker exec valheim bash`

**NOTE**: referencing containers by name is only possible if you specify the `--name` flag in your docker run command.

## Helm
* `git clone https://gitlab.com/japtain_cack/valheim-server.git`
* `cp valheim-server/helm_chart/values.yaml valheim_config.yaml`
* Customize `valheim_config.yaml` as necessary.
* Install: `helm install -n valheim valheim-world1 valheim-server/helm_chart -f valheim_config.yaml`
* Upgrade: `helm upgrade -n valheim valheim-world1 valheim-server/helm_chart -f valheim_config.yaml`
* **Keep your `valheim_config.yaml` in git**.

# Configuration
## Reference files
* [start.sh](https://gitlab.com/japtain_cack/valheim-server/-/blob/master/remco/templates/start.sh)

## Faster startup, no steamCMD validation
SteamCMD validation is disabled by default, it was causing the server to take a lot more time on boot.
Valheim already takes FOREVER to boot, so this is disabled by default.

To enable SteamCMD file validation, in case you need to re-validate your pod/container data. This can
be enabled on demand by adding an environment variable `STEAMCMD_VALIDATE` to your helm values or docker run
command.
 
This project uses [Remco config management](https://github.com/HeavyHorst/remco) and 
[envtpl](https://github.com/envtpl/envtpl).
This allows for templatization of config files and options can be set using environment variables.
This allows for easier deployments using most docker orchistration/management platforms including Kubernetes.

## Environment Variables
### Set user and/or group id (optional)
* `VALHEIM_UID=10000`
* `VALHEIM_GUID=10000`

### Enable crossplay
Pasting from Valheim Discord channel:
* Players can now use the parameter ‘-crossplay’ while running Dedicated servers to support crossplay.
  When using this parameter, the backend will be running Playfab instead of Steamworks. (A ‘Join code’
  will appear when you join a Dedicated server supporting crossplay. Players can use this code to join
  the server. This Join code is regenerated whenever the server is restarted.)
* A new branch, "default_old", has been added, where you can play on the previous Steam version of the game
  in case your server hasn't been updated yet (Steam might need a restart for the branch to show up)

* `VALHEIM_CROSSPLAY=true`

### Enable Valheim Plus mod
* Use `latest` or pin a specific version. Use only one environment variable below, I'm just showing you
  how to define `latest`, or a specific version.
  * `VALHEIM_PLUS_VERSION=latest`
  * `VALHEIM_PLUS_VERSION=0.9.9.11`
* `vim server/BepInEx/config/valheim_plus.cfg`

### Statically defined environment variables
Let's start by looking at `start.sh`. You should see something like this `-name` "{{ getv("/valheim/name", "0.0.0.0") }}"`.
`start.sh` uses statically defined keys, meaning I typed them out fully. Let's break this down.
* `-name` is simply static text, in this example we are setting the `-name` option.
* `{{ ... }}`. Everything in the brackets will be rendered using the templating language processor. 
* `getv("/key", "default_value")` is a function that takes two arguments. This looks up the environment variable based on the
  key, `"/valheim/name"`, and sets the value. If there is no value set in the env, the default is used.
* `"/valheim/name"` this is a key, which maps to an environment variable. Envs are all CAPS and separated by underscores.
  `"/valheim/name"` becomes `VALHEIM_NAME`. Simple.

