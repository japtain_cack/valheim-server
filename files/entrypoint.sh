#!/usr/bin/env bash

export REMCO_RESOURCE_DIR=${REMCO_HOME}/resources.d
export REMCO_TEMPLATE_DIR=${REMCO_HOME}/templates
set -x

echo "#####################################"
date
echo

# Ensure proper permissions
chown -Rv valheim:valheim ${VALHEIM_HOME}

# Steam validation logic
if [[ ! -z $VALHEIM_STEAM_VALIDATE ]]; then
  if [[ ! "$VALHEIM_STEAM_VALIDATE" =~ true|false ]]; then
    echo '[ERROR] VALHEIM_STEAM_VALIDATE must be true or false'
    exit 1
  elif [[ "$VALHEIM_STEAM_VALIDATE" == true ]]; then
    VALHEIM_STEAM_VALIDATE_VALUE="validate"
  else
    VALHEIM_STEAM_VALIDATE_VALUE=""
  fi
fi

# SteamCMD configuration file
cat <<EOF> ${VALHEIM_HOME}/valheim.conf
@ShutdownOnFailedCommand 1 //set to 0 if updating multiple servers at once
@NoPromptForPassword 1
force_install_dir ${VALHEIM_HOME}/server/
login anonymous
app_update 896660 ${VALHEIM_STEAM_VALIDATE_VALUE}
quit
EOF

# Install the game server via steam
./steamcmd.sh +runscript ${VALHEIM_HOME}/valheim.conf

# Run remco configuration management
remco

# Install ValheimPlus
if [[ "$VALHEIM_PLUS_VERSION" == "" ]]; then
  echo VALHEIM_PLUS_VERSION not defined, skipping ValheimPlus installation...
  echo
else
  echo VALHEIM_PLUS_VERSION: $VALHEIM_PLUS_VERSION
  echo
  if [[ "$VALHEIM_PLUS_VERSION" == "latest" ]]; then
    wget -O UnixServer.zip https://github.com/valheimPlus/ValheimPlus/releases/latest/download/UnixServer.zip
  else
    wget -O UnixServer.zip https://github.com/valheimPlus/ValheimPlus/releases/download/${VALHEIM_PLUS_VERSION}/UnixServer.zip
  fi
  unzip -o -d ${VALHEIM_HOME}/server/ UnixServer.zip
  rm -f UnixServer.zip
fi

echo
echo "#####################################"

cd ${VALHEIM_HOME}/server/
if [[ "$VALHEIM_PLUS_VERSION" == "" ]]; then
  echo starting vanilla valheim server...
else
  echo starting valheim plus server...
  chmod ug+x start_server_bepinex.sh
fi
./start.sh
echo
echo

