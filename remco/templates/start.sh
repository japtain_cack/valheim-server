export templdpath=$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=./linux64:$LD_LIBRARY_PATH
export SteamAppId=892970

{% if exists("/valheim/plus/version") %}
./start_server_bepinex.sh -nographics -batchmode \
{% else %}
./valheim_server.x86_64 -nographics -batchmode \
{% endif %}
  -name "{{ getv("/valheim/name", "valheim") }}" \
  -port "{{ getv("/valheim/port", "2456") }}" \
  -world "{{ getv("/valheim/world", "valheim") }}" \
  -password "{{ getv("/valheim/password", "secret123") }}" \
  -public "{{ getv("/valheim/public", "1") }}" \
{% if getv("/valheim/crossplay", "false") == "true" %}
  -crossplay \
{% endif %}
  -savedir "{{ getv("/valheim/savedir", "/home/valheim/server/worlds") }}"

export LD_LIBRARY_PATH=$templdpath
