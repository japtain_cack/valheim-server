# Build remco from specific commit
######################################################
FROM golang AS remco

# remco (lightweight configuration management tool) https://github.com/HeavyHorst/remco
RUN go install github.com/HeavyHorst/remco/cmd/remco@latest


# Build valheim base
######################################################
FROM ubuntu:oracular AS valheim-base
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV VALHEIM_HOME=/home/valheim
ENV VALHEIM_UID=10000
ENV VALHEIM_GUID=10000

RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
    unzip \
    curl \
    wget \
    vim \
    gnupg2

RUN apt-get -y install \
    lib32gcc-s1 \
    libpulse0 \
    libpulse-dev \
    libatomic1 \
    libc6

RUN groupadd -g $VALHEIM_GUID valheim && \
    useradd -l -s /bin/bash -d $VALHEIM_HOME -m -u $VALHEIM_UID -g valheim valheim && \
    passwd -d valheim

## Install remco
COPY --from=remco /go/bin/remco /usr/local/bin/remco
COPY --chown=root:root remco /etc/remco
RUN chmod -R 0775 /etc/remco

## Install SteamCMD
RUN curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - -C ${VALHEIM_HOME}/ && \
    chmod ugo+x ${VALHEIM_HOME}/steamcmd.sh

## Install entrypoint and update permissions
COPY --chown=valheim:valheim files/entrypoint.sh ${VALHEIM_HOME}/
RUN chmod ugo+x ${VALHEIM_HOME}/entrypoint.sh && \
    chown -Rv valheim:valheim ${VALHEIM_HOME}


# Build valheim image
######################################################
FROM valheim-base as valheim

ARG CI_COMMIT_AUTHOR
ARG CI_COMMIT_TIMESTAMP
ARG CI_COMMIT_SHA
ARG CI_COMMIT_TAG
ARG CI_PROJECT_URL

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV VALHEIM_HOME=/home/valheim
ENV VALHEIM_STEAM_VALIDATE="false"
ENV STEAMAPPID=896660
ENV REMCO_HOME=/etc/remco

LABEL maintainer=$CI_COMMIT_AUTHOR
LABEL author=$CI_COMMIT_AUTHOR
LABEL description="Valheim dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="registry.gitlab.com/japtain_cack/valheim-server"
LABEL org.label-schema.description="Valheim dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.url=$CI_PROJECT_URL
LABEL org.label-schema.vcs-url=$CI_PROJECT_URL
LABEL org.label-schema.vcs-ref=$CI_COMMIT_SHA
LABEL org.label-schema.version=$CI_COMMIT_TAG
LABEL org.label-schema.build-date=$CI_COMMIT_TIMESTAMP

WORKDIR $VALHEIM_HOME
VOLUME "${VALHEIM_HOME}/server"
EXPOSE 2456-2458/tcp
EXPOSE 2456-2458/udp

USER valheim
ENTRYPOINT ["./entrypoint.sh"]

